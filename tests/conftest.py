import pytest

import kivalu


@pytest.fixture(scope="module")
def server():
    with kivalu.TestServer() as server:
        yield server
